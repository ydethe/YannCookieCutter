from cookiecutter.main import cookiecutter


param = {
    "full_name": "Yann de The",
    "email": "ydethe@gmail.com",
    "gitlab_username": "ydethe",
    "project_name": "MyAwesomeProject",
    "project_short_description": "Python Boilerplate contains all the boilerplate you need to create a Python package.",
    "command_line_interface": "y",
}

cookiecutter(".", no_input=True, extra_context=param, overwrite_if_exists=True)
