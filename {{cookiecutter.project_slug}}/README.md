# Quick look

{{cookiecutter.project_short_description}}

# Build the doc

Just run:

    pdm doc

This will create the doc in build/htmldoc

A few guidelines for updating the doc
https://sphinxcontrib-napoleon.readthedocs.io/en/latest/example_google.html
